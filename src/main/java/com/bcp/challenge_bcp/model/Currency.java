package com.bcp.challenge_bcp.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "currency")
public class Currency {

    private long id;
    private String nombre;
    private BigDecimal factor;
    private String simbolo;

    public Currency() {

    }

    public Currency(String nombre) {
        this.nombre = nombre;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "nombre", nullable = false)
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name = "factor", nullable = false)
    public BigDecimal getFactor() {
        return factor;
    }
    public void setFactor(BigDecimal factor) {
        this.factor = factor;
    }
    
    @Column(name = "simbolo", nullable = false)
    public String getSimbolo() {
        return simbolo;
    }
    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }
    
}