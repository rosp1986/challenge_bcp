package com.bcp.challenge_bcp.bean;

import java.math.BigDecimal;

public class Exchange {
	
	private BigDecimal montoOrigen;
	private BigDecimal montoCambio;
	private String monedaOrigen;
	private String monedaDestino;
	private BigDecimal factor;
	
	public Exchange() {
		
	}
	
	public BigDecimal getMontoOrigen() {
		return montoOrigen;
	}

	public void setMontoOrigen(BigDecimal montoOrigen) {
		this.montoOrigen = montoOrigen;
	}

	public BigDecimal getMontoCambio() {
		return montoCambio;
	}

	public void setMontoCambio(BigDecimal montoCambio) {
		this.montoCambio = montoCambio;
	}

	public String getMonedaOrigen() {
		return monedaOrigen;
	}

	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}

	public String getMonedaDestino() {
		return monedaDestino;
	}

	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}

	public BigDecimal getFactor() {
		return factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}
}
