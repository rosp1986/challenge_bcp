package com.bcp.challenge_bcp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bcp.challenge_bcp.model.Currency;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Long>{
	
	@Query(value = "SELECT * FROM currency c WHERE c.simbolo = :simbolo", nativeQuery = true)
	Optional <Currency> findBySimbolo(@Param("simbolo") String simbolo);
	
}