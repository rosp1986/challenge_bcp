package com.bcp.challenge_bcp.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bcp.challenge_bcp.bean.Exchange;
import com.bcp.challenge_bcp.exception.ResourceNotFoundException;
import com.bcp.challenge_bcp.model.Currency;
import com.bcp.challenge_bcp.repository.CurrencyRepository;

@RestController
@RequestMapping("/api/v1")
public class CurrencyController {
    @Autowired
    private CurrencyRepository currencyRepository;

    @GetMapping("/currencies")
    public List < Currency > getAllcurrencys() {
        return currencyRepository.findAll();
    }
    
    @GetMapping("/currencies/simbolo")
    public ResponseEntity < Currency > findBySimbolo(@Valid @RequestBody Currency currency)
    throws ResourceNotFoundException {
    	Currency currencyDetails =  currencyRepository.findBySimbolo(currency.getSimbolo())
    			.orElseThrow(() -> new ResourceNotFoundException("Moneda no encontrada para el simbolo :: " + currency.getSimbolo()));
    	return ResponseEntity.ok().body(currencyDetails);
    }    

    @GetMapping("/currencies/{id}")
    public ResponseEntity < Currency > getcurrencyById(@PathVariable(value = "id") Long currencyId)
    throws ResourceNotFoundException {
        Currency currency = currencyRepository.findById(currencyId)
            .orElseThrow(() -> new ResourceNotFoundException("Moneda no encontrada para el id :: " + currencyId));
        return ResponseEntity.ok().body(currency);
    }

    @PostMapping("/currencies")
    public Currency createcurrency(@Valid @RequestBody Currency currency) {
        return currencyRepository.save(currency);
    }

    @PutMapping("/currencies/{id}")
    public ResponseEntity < Currency > updatecurrency(@PathVariable(value = "id") Long currencyId,
        @Valid @RequestBody Currency currencyDetails) throws ResourceNotFoundException {
        Currency currency = currencyRepository.findById(currencyId)
            .orElseThrow(() -> new ResourceNotFoundException("Moneda no encontrada para el id :: " + currencyId));

        currency.setNombre(currencyDetails.getNombre());
        currency.setFactor(currencyDetails.getFactor());
        currency.setSimbolo(currencyDetails.getSimbolo());
        final Currency updatedcurrency = currencyRepository.save(currency);
        return ResponseEntity.ok(updatedcurrency);
    }

    @DeleteMapping("/currencies/{id}")
    public Map < String, Boolean > deletecurrency(@PathVariable(value = "id") Long currencyId)
    throws ResourceNotFoundException {
        Currency currency = currencyRepository.findById(currencyId)
            .orElseThrow(() -> new ResourceNotFoundException("Moneda no encontrada para el id :: " + currencyId));

        currencyRepository.delete(currency);
        Map < String, Boolean > response = new HashMap < > ();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
    
    @GetMapping("/currencies/exchange")
    public ResponseEntity < Exchange > findBySimbolo(@Valid @RequestBody Exchange exchangeDetails)
    throws ResourceNotFoundException {
    	Currency currencyOrigen = currencyRepository.findBySimbolo(exchangeDetails.getMonedaOrigen())
    			.orElseThrow(() -> new ResourceNotFoundException("Moneda no encontrada para el simbolo :: " + exchangeDetails.getMonedaOrigen()));
    		
    	Currency currencyDestino = currencyRepository.findBySimbolo(exchangeDetails.getMonedaDestino())
    			.orElseThrow(() -> new ResourceNotFoundException("Moneda no encontrada para el simbolo :: " + exchangeDetails.getMonedaDestino()));

    	BigDecimal factor = currencyOrigen.getFactor().divide(currencyDestino.getFactor(), 2, RoundingMode.HALF_UP);
    	Exchange exchange =  new Exchange();
    	exchange.setMontoOrigen(exchangeDetails.getMontoOrigen());
    	exchange.setMontoCambio(exchangeDetails.getMontoOrigen().multiply(factor).setScale(2, RoundingMode.HALF_UP));
    	exchange.setMonedaOrigen(exchangeDetails.getMonedaOrigen());
    	exchange.setMonedaDestino(exchangeDetails.getMonedaDestino());
    	exchange.setFactor(factor);
    	
    	return ResponseEntity.ok().body(exchange);
    } 
    
}