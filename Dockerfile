FROM openjdk:8u151-slim

VOLUME /tmp
ADD challenge_bcp.jar /challenge_bcp.jar

EXPOSE 8080
EXPOSE 8081

RUN sh -c 'touch /challenge_bcp.jar'

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/challenge_bcp.jar"]
